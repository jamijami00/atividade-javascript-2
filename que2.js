function multiMatriz (matrizA, matrizB){
    let resultado = [];

    if (matrizA.length == matrizB.length){ 
        for (let i = 0; i < matrizA.length; i++) {
            resultado[i] = [];
            
            for (let j = 0; j < matrizB[0].length; j++) {
                let soma = 0;
    
                for (let k = 0; k < matrizA[0].length; k++) {
                    soma += matrizA[i][k] * matrizB[k][j];   
                }
                resultado[i][j] = soma;
            }
        }
        return resultado;
    }
    console.log('Tamanhos diferentes! Abortando Operação!');
    return null;
}

//Teste 1
let m1 = [[2,-1],[2,0]]
let m2 = [[2,3],[-2,1]];

let mResult = multiMatriz(m1,m2);
console.log('Teste 1: '+mResult);
console.log(' ');

//Teste 2
let m3 = [[4,0],[-1,-1]];
let m4 = [[-1,3],[2,7]];

mResult = multiMatriz(m3,m4);
console.log('Teste 2: '+mResult);
console.log(' ');

//Teste 3
let m5 = [[1,3],[-6,3]];
let m6 = [[0,1],[4,8],[8,6]];

mResult = multiMatriz(m5,m6);
console.log('Teste 3: '+mResult);