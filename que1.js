function geraArray (tamanho){
    let x = [];

    //Gera o Array
    for (var i = 0; i < tamanho ; i++){
        x.push((Math.ceil(Math.random() * (100 - 0) + 0)));
    }
    
    //Sort no Array usando .sort()
    //Nesse caso, se o resultado de a - b for menor que 0, a é menor, logo vem primeiro. 
    //Ex: 3 - 9 = -6, o que significa que 3 vem antes de 9.  
    //x.sort((a,b) => a - b);

    //Sort Manual por Selection
    selectionSort(x);

    //Imprime o Array
    console.log(x);
}

function selectionSort(arr){

    for (let i = 0; i < arr.length; i++) {
        let min = i;
        for (let j = i+1; j < arr.length; j++) {
            if(arr[j] < arr[min]) { min=j; }
        }
        var temp = arr[i];
        arr[i] = arr[min];
        arr[min] = temp;
    }
    return arr;
}

//Resposta
geraArray((Math.ceil(Math.random() * 10)));
